import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Sidebar from 'components/Sidebar';
import { renderRoutes } from 'react-router-config';
import { logoutUser } from '../../redux/actions/auth';
import { redirectTo } from '../../libs/common';
import './Sidebar.scss';
import Button from '../../components/Button';
import endpoints from '../../routes/endpoints';

function SidebarLayout(props) {
  const { route } = props;
  function handleLogout() {
    props.logoutUser(() => {
      redirectTo(props.history, endpoints.login);
    });
  }
  return (
      <div className="SidebarLayout">
        <div className="SidebarLayout__Menu">
          <div className="SidebarLayout__Sidebar__Title">
            <span className="SidebarLayout__Sidebar__Title__Heading">
              MayaEdu
            </span>
            <br />
            <span className="SidebarLayout__Sidebar__Title__HeadingName">
            For Medical Students
            </span>
        </div>
        <div className="SidearLayoutMenu__UserSetting">
          <Button
            color="red"
            onClick={handleLogout}
            className="btn btn-outline-danger">Logout
          </Button>
        </div>
        </div>
      <div className="Display--Flex">
        <div className="SidebarLayout__Sidebar">
          <Sidebar />
        </div>
        <div className="SidebarLayout__Content">
          {renderRoutes(route.routes)}
        </div>
        </div>
      </div>
  );
}

SidebarLayout.propTypes = {
  route: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps, { logoutUser })(React.memo(SidebarLayout));
