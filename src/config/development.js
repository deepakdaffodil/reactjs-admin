const API = {
  BASE_URL: '',
  WEB_API_KEY: ''
};

const DEV_TOOLS = {
  enableReduxDevTools: true,
  logError: true
};

module.exports = {
  API,
  DEV_TOOLS
};
