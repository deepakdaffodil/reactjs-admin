import React from 'react';
import PropTypes from 'prop-types';
import './Sidebar.scss';

function Sidebar({ className }) {
  return (
    <div className={`Sidebar ${className}`}>
      <div height="100%">
        <div pad="small" className="Border-Bottom--Gray Card">
          Manage Clinical Cases
        </div>
        <div pad="small" className="Border-Bottom--Gray Card">
          Manage Active Cases
        </div>
        <div pad="small" className="Border-Bottom--Gray Card">
          Manage Study Material
        </div>
        <div pad="small" className="Border-Bottom--Gray Card">
          Manage Diagnosis
        </div>
        <div pad="small" className="Border-Bottom--Gray Card">
          Manage Sub Admin
        </div>
      </div>
    </div>
  );
}

Sidebar.propTypes = {
  className: PropTypes.string
};

Sidebar.defaultProps = {
  className: ''
};

export default React.memo(Sidebar);
